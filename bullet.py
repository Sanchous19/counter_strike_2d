import pygame
import math
import random
import sprite
import pickle


class Bullet:
    def __init__(self, index=0, x=0, y=0, angle=0, speed=30):
        self.index = index
        self.x = x
        self.y = y
        self.x_speed = speed * math.cos(math.radians(angle))
        self.y_speed = -speed * math.sin(math.radians(angle))
        self.angle = angle
        self.speed = speed

    def move(self):
        self.x += self.x_speed
        self.y += self.y_speed

    def display(self, screen):
        screen.blit(sprite.bullet_image, (self.x, self.y))

    # def encode(self):
    #     return (str(self.name) + '#' + str(self.x) + '#' + str(self.y) + '#' + str(self.angel)).encode('ascii')
    #
    # def decode(self, data):
    #     data = data.split('#')
    #     self.name = data[0]
    #     self.x = float(data[1])
    #     self.y = float(data[2])
    #     self.angel = float(data[3])
    #     self.x_speed = self.speed * math.cos(math.radians(self.angel))
    #     self.y_speed = -self.speed * math.sin(math.radians(self.angel))

    def is_empty(self):
        if self.x == 0 and self.y == 0 and self.angle == 0:
            return True
        else:
            return False

    def serialize(self):
        return pickle.dumps((self.x, self.y, self.angle, self.index))

    def deserialize(self, data):
        self.x, self.y, self.angle, self.index = pickle.loads(data)
        self.x_speed = self.speed * math.cos(math.radians(self.angle))
        self.y_speed = -self.speed * math.sin(math.radians(self.angle))
