from tkinter import *
from tkinter import messagebox
import threading
import socket
import control_socket
from control_socket import Server, Client
import game
import screen_info as scr
import sprite
import time
import mongo_work


server = None
client = None
thread = None
is_connection = True
server_thread = None


def on_closing(window):
    global server, client, thread, is_connection
    print("on_closing")
    if isinstance(thread, threading.Thread):
        print('thread stop')
        is_connection = False
        thread.join()
        thread = None
    if isinstance(server, Server):
        print('server stop')
        server.close_socket()
        server = None
    if isinstance(client, Client):
        print('client stop')
        client.close_socket()
        client = None
    window.destroy()


def select(side, window, button1, button2):
    global thread
    button1.configure(state=DISABLED)
    button2.configure(state=DISABLED)
    thread = threading.Thread(target=selection(side, window))
    thread.start()
    # messagebox.showinfo('', 'Подождите пока все выберут сторону')


def selection(side, window):
    print(side)
    client.send(side)
    client.sock.setblocking(True)
    is_ready = client.receive_prepared_message()
    if is_ready:
        window.destroy()
        print('The game start')
        game.start_game(server, client)
    print('exit from selection')


def connection(label):
    global is_connection
    server.sock.settimeout(1)
    while is_connection:
        try:
            server.receive_request()
            if not isinstance(client, Client):
                label.config(text=str(len(server.addresses)))
        except socket.timeout:
            print('no connection')
    is_connection = True


def waiting(button):
    global client, is_connection
    client.sock.settimeout(1)
    while is_connection:
        try:
            is_ready, kind = client.receive_prepared_message()
            if is_ready:
                if kind:
                    is_connection = False
                    button.invoke()
                    break                                       # Start game
                else:
                    print('qwerqyquioepiuyertruytweyqtruytewuytruqewytruytwqueyrtuyqtweruqewtr')
                    game.start_game(server, client)
            else:
                messagebox.showinfo('Проблемы на сервере', 'Сервер отключился')
                client = None
                return
        except socket.timeout:
            print('waiting')
            pass
    is_connection = True
    print('exit from waiting')


def connect(button, address):
    global client, thread
    if type(client) is Client:
        return                                                              # connect
    client = Client(address)
    if not client.is_correct_address:
        client = None
        messagebox.showinfo('Ошибка с адресом', 'Сервер с таким адресом не обнаружен')
        return
    is_send_request = True
    while is_send_request:
        is_good = client.send_request(True)
        if is_good:
            thread = threading.Thread(target=lambda: waiting(button))
            thread.start()
            messagebox.showinfo('Успешное подключение', 'Дождитесь сервера')
            break
        else:
            is_send_request = messagebox.askokcancel('Ошибка при подключении',
                                                     'Хотите ещё раз попробовать подключиться к этому адресу?')
            if not is_send_request:
                client.close_socket()
                client = None


def load_game(window):
    global client, is_connection, thread, server_thread
    if len(server.addresses) != mongo_work.get_players_number() - 1:
        messagebox.showinfo('Ошибка', 'Количество человек не соответствует количеству игроков в сохраненной игре')
        return
    window.destroy()
    server.send_invitation(False)
    client = Client('127.0.0.1')
    is_good = False
    while not is_good:
        is_good = client.send_request(True)
    is_connection = False
    thread.join()
    thread = None
    print('open_selection_menu')
    server_thread = threading.Thread(target=server.load_game)
    server_thread.start()
    game.start_game(server, client)


def open_selection_menu(window):
    window.destroy()
    global client, is_connection, thread, server_thread
    if isinstance(server, Server):
        # if len(server.addresses) == 0:
        #     messagebox.showinfo('', 'Вы не можете играть один')
        #     return
        server.send_invitation(True)
        client = Client('127.0.0.1')
        is_good = False
        while not is_good:
            is_good = client.send_request(True)
        is_connection = False
        thread.join()
        thread = None
        print('open_selection_menu')
        server_thread = threading.Thread(target=server.wait_selection)
        server_thread.start()
    print('open_selection_menu2')
    selection_menu = Toplevel()
    selection_menu['bg'] = '#11191f'
    selection_menu.geometry('{0}x{1}+0+0'.format(scr.player_width, scr.player_height))
    counter_terrorist_label = Label(selection_menu, text='Спецназ', font='Arial 40', fg='white', bg='#11191f')
    counter_terrorist_label.place(relx=0.18, rely=0.03)
    counter_terrorist_image = PhotoImage(file='images/counter_terrorist_select1.png')
    counter_terrorist_button = Button(selection_menu, image=counter_terrorist_image,
                                      command=lambda: select('counter terrorist', selection_menu,
                                                             counter_terrorist_button, terrorist_button))
    counter_terrorist_button.place(relx=0.05, rely=0.1)
    counter_terrorist_label = Label(selection_menu, text='Террористы', font='Arial 40', fg='white', bg='#11191f')
    counter_terrorist_label.place(relx=0.67, rely=0.03)
    terrorist_image = PhotoImage(file='images/terrorist_select1.png')
    terrorist_button = Button(selection_menu, image=terrorist_image,
                              command=lambda: select('terrorist', selection_menu,
                                                     counter_terrorist_button, terrorist_button))
    terrorist_button.place(relx=0.53, rely=0.1)
    label = Label(selection_menu, text='Выберите сторону и подождите остальных', font='Arial 40', fg='white',
                  bg='#11191f')
    label.pack(side=BOTTOM)
    selection_menu.mainloop()


def open_create_menu():
    global server, thread
    server = Server()
    print(server.sock.getsockname())
    create_menu = Toplevel()
    create_menu.geometry('640x360+{}+{}'.format(scr.player_width // 2 - 320, scr.player_height // 2 - 180))
    screen_saver_image = PhotoImage(file='images/screen_saver2.png')
    background_label = Label(create_menu, image=screen_saver_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    address_label = Label(create_menu, text='Ваш ip адрес: ' + control_socket.get_ip_address(), font='Arial 25', fg='white',
                          bg='#030404')
    address_label.place(x=130, y=10)
    label = Label(create_menu, text='Подключившихся игроков', font='Arial 20', fg='white', bg='#24313d')
    label.place(x=160, y=110)
    player_number_label = Label(create_menu, text=0, font='Arial 20', fg='white', bg='#24313d')
    player_number_label.place(x=300, y=150)
    create_button = Button(create_menu, text='Создать игру', font='Arial 20', fg='white', bg='#202a34',
                           activebackground='#202a34', command=lambda: open_selection_menu(create_menu))
    create_button.place(x=220, y=200)
    load_button = Button(create_menu, text='Загрузить игру', font='Arial 20', fg='white', bg='#202a34',
                           activebackground='#202a34', command=lambda: load_game(create_menu))
    load_button.place(x=210, y=270)
    thread = threading.Thread(target=connection, args=[player_number_label])
    thread.start()
    create_menu.protocol("WM_DELETE_WINDOW", lambda: on_closing(create_menu))
    create_menu.mainloop()


def open_connect_menu():
    connect_menu = Toplevel()
    screen_saver_image = PhotoImage(file='images/screen_saver2.png')
    background_label = Label(connect_menu, image=screen_saver_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    open_selection_menu_button = Button(command=lambda: open_selection_menu(connect_menu))
    connect_menu.geometry('640x360+{}+{}'.format(scr.player_width // 2 - 320, scr.player_height // 2 - 180))
    # address_label = Label(connect_menu, text=control_socket.get_ip_address())
    # address_label.pack()
    address_entry = Entry(connect_menu, font='Arial 20')
    address_entry.place(x=220, y=100, width=210)
    connect_button = Button(connect_menu, text='Подключиться', font='Arial 25', fg='white', bg='#202a34',
                            activebackground='#202a34',
                            command=lambda: connect(open_selection_menu_button, address_entry.get()))
    connect_button.place(x=200, y=170)
    connect_menu.protocol("WM_DELETE_WINDOW", lambda: on_closing(connect_menu))
    connect_menu.mainloop()


def main():
    initial_menu = Tk()
    initial_menu.title('Counter Strike')
    screen_saver_image = PhotoImage(file='images/screen_saver.png')
    background_label = Label(image=screen_saver_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    initial_menu.geometry('960x540+{}+{}'.format(scr.player_width // 2 - 480, scr.player_height // 2 - 270))
    open_create_menu_button = Button(initial_menu, text='Создать игру', font='Arial 30', fg='white', bg='#332f26',
                                     activebackground='#332f26', command=open_create_menu)
    open_create_menu_button.place(x=30, y=370)
    open_connect_menu_button = Button(initial_menu, text='Подключиться', font='Arial 30', fg='white', bg='#332f26',
                                      activebackground='#332f26', command=open_connect_menu)
    open_connect_menu_button.place(x=30, y=450)
    initial_menu.protocol("WM_DELETE_WINDOW", lambda: on_closing(initial_menu))
    initial_menu.mainloop()


if __name__ == '__main__':
    mongo_work.load()
    main()
