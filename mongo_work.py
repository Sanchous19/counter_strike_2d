import pymongo
import game


def save():
    mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
    database = mongo_client['mydatabase']
    players = database['players']
    players.delete_many({})
    for pl in game.players:
        save_characteristics = {'side': pl.side, 'x_center': pl.x_center, 'y_center': pl.y_center, 'angle': pl.angle,
                                'health': pl.health}
        players.insert_one(save_characteristics)
    characteristics = database['characteristics']
    characteristics.delete_many({})
    characteristics.insert_one({'players_number': len(game.players), 'terrorist_number_wins': game.terrorist_number_wins,
                                'counter_terrorist_number_wins': game.counter_terrorist_number_wins})
    print('MONGO')


# myquery = { "address": "Valley 345" }
# newvalues = { "$set": { "address": "Canyon 123" } }
#
# mycol.update_one(myquery, newvalues)

def load():
    mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
    database = mongo_client['mydatabase']
    players = database['players']
    players_result = []
    for pl in players.find():
        pl_inf = (pl['side'], pl['x_center'], pl['y_center'], pl['angle'], pl['health'])
        players_result.append(pl_inf)
    characteristics = database['characteristics']
    terrorist_number_wins = characteristics.find_one({}, {'terrorist_number_wins': 1})
    counter_terrorist_number_wins = characteristics.find_one({}, {'counter_terrorist_number_wins': 1})
    characteristics_result = (terrorist_number_wins['terrorist_number_wins'],
                              counter_terrorist_number_wins['counter_terrorist_number_wins'])
    print('MONGO')
    print(characteristics_result)
    print(players_result)
    return characteristics_result, players_result


def print_mongo():
    mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
    database = mongo_client['mydatabase']
    players = database['players']
    print(players)
    characteristics = database['characteristics']
    print(characteristics)


def get_players_number():
    mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
    database = mongo_client['mydatabase']
    characteristics = database['characteristics']
    players_number = characteristics.find_one({}, {'players_number': 1})
    return players_number['players_number']
    # print(x['players_number'])
    # players = database['players']
    # for x in players.find():
    #     # y = x.find_one({}, {'side': 1})
    #     print(x['x_center'])
