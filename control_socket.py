from socket import *
from player import Player
from bullet import Bullet
import random
import game
import pickle
import time
import mongo_work
import threading


def get_ip_address():
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address


class Server:
    def __init__(self):
        self.sock = socket(AF_INET, SOCK_DGRAM)
        self.sock.bind(('', 13579))
        self.bul_sock = socket(AF_INET, SOCK_DGRAM)
        self.bul_sock.bind(('', 13597))
        self.players_number = 0
        self.addresses = []
        self.players_characteristics = []
        self.sides = []
        self.killed = []
        self.bullets = [[]] * self.players_number
        self.terrorist_number = 0
        self.counter_terrorist_number = 0
        self.terrorist_number_wins = 0
        self.counter_terrorist_number_wins = 0
        self.received_number = 0
        self.is_receive_characteristics = False
        self.is_receive_bullet = True
        self.bullet_addresses = []

    def send_characteristics(self, address):
        # pl_data = b''
        # for player in self.players_characteristics:
        #     pl_data += player.encode('ascii') + b'&'
        # pl_data = pl_data[:len(pl_data) - 1]
        # # print(pl_data, address)
        # bul_inf = b''
        # index = self.addresses.index(address)
        # for i in range(self.players_number):
        #     if index == i:
        #         continue
        #     for bul in self.bullets[i]:
        #         bul_inf += bul.encode + '&'
        # if bul_inf != '':
        #     bul_inf = bul_inf[:len(bul_inf) - 1]
        # data = pl_data + b'&&&' + bul_inf

        data = []
        for character in self.players_characteristics:
            data.append(character)
        # data = (index, player_data, bullet_data)
        # print(index, pickle.loads(player_data), pickle.loads(bullet_data))
        data = pickle.dumps(data)
        self.sock.sendto(data, address)
        # data = []
        # for character in self.players_characteristics:
        #     data.append(character)
        # data = pickle.dumps(data)

    def receive_characteristics(self):
        self.sock.setblocking(True)
        self.is_receive_characteristics = True
        while self.is_receive_characteristics:
            (data, address) = self.sock.recvfrom(1024)
            try:
                if data.decode('utf-8') == 'KILL':
                    self.send_characteristics(address)
                    index = self.addresses.index(address)
                    print(index, 'index')
                    if not self.killed[index]:
                        self.killed[index] = True
                        if self.sides[index] == 'terrorist':
                            self.terrorist_number -= 1
                        else:
                            self.counter_terrorist_number -= 1
                        is_finish = self.is_finish_round()
                        print('is_finish', is_finish)
                        if is_finish:
                            print(12345)
                            self.send_start_coordinates()
            # elif True:
            #     data = pickle.loads(data)
            #     print(data)
            #     return
            except UnicodeDecodeError:
                index = self.addresses.index(address)
                self.players_characteristics[index] = data
                self.send_characteristics(address)
                # data = data.split('&&&')
                # pl_inf = data[0]
                # new_bul_inf = data[1].split('&')
                # del_bul_inf = data[2].split('&')
                # # print(pl_inf)
                # for pl in self.players_characteristics:
                #     if pl.decode(pl_inf):
                #         break
                # index = self.addresses.index(address)
                # if del_bul_inf[0] != '':
                #     print(del_bul_inf)
                #     j = 0
                #     for bul_inf in del_bul_inf:
                #         bul_name = bul_inf.split('#')[0]
                #         while self.bullets[index][j].name != bul_name:
                #             j += 1
                #         self.bullets[index].remove(j)
                # if new_bul_inf[0] != '':
                #     for bul_inf in new_bul_inf:
                #         new_bullet = Bullet()
                #         new_bullet.decode(bul_inf)
                #         self.bullets[index].append(new_bullet)

                # index = self.addresses.index(address)
                # self.players_characteristics[index] = data

            # if self.is_finish_round(address):
            #     self.received_number += 1
            #     if self.received_number == self.players_number:
            #         self.send_start_coordinates()                                 NO COMMENTS
            #         self.received_number = 0
            # else:
            #
            #     player_data, bullet_data = pickle.loads(data)
            #     if address == self.addresses[0]:
            #         print(address, pickle.loads(player_data))
            #     self.send_characteristics(address, player_data, bullet_data)
            #
                # i
                # send_thread = threading.Thread(target=lambda: self.send_characteristics(address, player_data, bullet_data))
                # send_thread.start()
            # self.send_characteristics(data, address)
        print('exit from server.receive_characteristics')

    def receive_bullet(self):
        self.is_receive_bullet = True
        self.bul_sock.setblocking(True)
        receive_number = 0
        while receive_number != self.players_number:
            data, address = self.bul_sock.recvfrom(1024)
            receive_number += 1
            self.bullet_addresses.append(address)
        while self.is_receive_bullet:
            data = self.bul_sock.recv(1024)
            for addr in self.bullet_addresses:
                self.bul_sock.sendto(data, addr)
        print('exit from server.receive_bullet')

    def send_start_coordinates(self):
        self.terrorist_number = 0
        self.counter_terrorist_number = 0
        self.killed = [False] * self.players_number
        for i in range(self.players_number):
            player = Player(self.sides[i])
            self.define_start_coordinates(player)
            self.players_characteristics[i] = player.serialize()
        data = []
        for character in self.players_characteristics:
            data.append(character)
        data = pickle.dumps(data)
        for addr in self.bullet_addresses:
            self.bul_sock.sendto(data, addr)

    def is_finish_round(self):
        data = b''
        is_finish = True
        if self.terrorist_number == 0:
            self.counter_terrorist_number_wins += 1
            data = b'COUNTER-TERRORIST WIN ROUND'
        elif self.counter_terrorist_number == 0:
            self.terrorist_number_wins += 1
            data = b'TERRORIST WIN ROUND'
        else:
            is_finish = False
        if self.terrorist_number_wins == 8:
            data = b'TERRORIST WIN GAME'
        elif self.counter_terrorist_number_wins == 8:
            data = b'COUNTER-TERRORIST WIN GAME'
        print(data, 'data')
        if is_finish:
            for addr in self.bullet_addresses:
                self.bul_sock.sendto(data, addr)
        return is_finish

    def receive_request(self):
        (request, address) = self.sock.recvfrom(1024)
        request = request.decode('utf-8')
        print(request)
        if request == 'Request for acceptance':
            try:
                self.addresses.index(address)
            except:
                self.addresses.append(address)
            self.sock.sendto(b'accept', address)
            print('receive_request', address)
        elif request == 'Request for exit':
            try:
                self.addresses.index(address)
                self.addresses.remove(address)
            except:
                pass
            self.sock.sendto(b'exit', address)
            print('receive_request', address)

    def send_invitation(self, is_create):
        if is_create:
            for addr in self.addresses:
                self.sock.sendto(b'Game will create', addr)
        else:
            for addr in self.addresses:
                print('debugggggg')
                self.sock.sendto(b'Game will load', addr)

    def wait_selection(self):
        self.players_number = len(self.addresses)
        self.players_characteristics = [''] * self.players_number
        self.sides = [''] * self.players_number
        self.sock.setblocking(True)
        is_no_terrorists, is_no_counter_terrorists = True, True
        selection_players_number = 0
        while selection_players_number != self.players_number:
            (message, address) = self.sock.recvfrom(1024)
            selection_players_number += 1
            side = message.decode('utf-8')
            if side == 'terrorist':
                is_no_terrorists = False
            else:
                is_no_counter_terrorists = False
            i = self.addresses.index(address)
            self.sides[i] = side
        if is_no_terrorists or is_no_counter_terrorists:
            i = random.randint(0, self.players_number - 1)
            self.sides[i] = 'terrorist' if is_no_terrorists else 'counter terrorist'
        health = [3] * self.players_number
        self.send_invitation(True)
        time.sleep(1)
        for i in range(self.players_number):
            data = pickle.dumps((i, self.sides, health))
            self.sock.sendto(data, self.addresses[i])
        for addr in self.bullet_addresses:
            self.bul_sock.sendto(b'Start the game', addr)
        self.send_start_coordinates()
        print('exit from server.wait selection')
        # data = ()
        # for character in self.players_characteristics:
        # init_information = b''
        # for player in self.players_characteristics:
        #     init_information += player.full_encode('ascii') + b'&&&'
        # init_information = init_information[:len(init_information) - 3]
        # print(init_information)
        # for addr in self.addresses:
        #     self.sock.sendto(init_information, addr)
        # self.bullets = [[]] * self.players_number

    def load_game(self):
        self.players_number = len(self.addresses)
        self.players_characteristics = [''] * self.players_number
        self.sides = [''] * self.players_number
        self.killed = [False] * self.players_number
        health = [3] * self.players_number
        characteristics, players_inf = mongo_work.load()
        self.terrorist_number_wins, self.counter_terrorist_number_wins = characteristics
        for i in range(self.players_number):
            self.sides[i] = players_inf[i][0]
            self.players_characteristics[i] = pickle.dumps((players_inf[i][1], players_inf[i][2], players_inf[i][3]))
            health[i] = players_inf[i][4]
        # self.send_invitation(False)
        time.sleep(1)
        for i in range(self.players_number):
            print('data', (i, self.sides, health))
            data = pickle.dumps((i, self.sides, health))
            self.sock.sendto(data, self.addresses[i])
        for addr in self.bullet_addresses:
            self.bul_sock.sendto(b'Start the game', addr)
        data = []
        for character in self.players_characteristics:
            data.append(character)
        data = pickle.dumps(data)
        for addr in self.bullet_addresses:
            self.bul_sock.sendto(data, addr)

    def define_start_coordinates(self, player):
        if player.side == 'terrorist':
            self.terrorist_number += 1
            player.x_center = 200
            player.y_center = 200 + 100 * self.terrorist_number
        else:
            self.counter_terrorist_number += 1
            player.x_center = 200
            player.y_center = 200 + 300 * self.counter_terrorist_number              ######################

    def close_socket(self):
        for addr in self.addresses:
            self.sock.sendto(b'Server disconnected', addr)
        self.sock.close()

    def finish_game(self):
        for addr in self.bullet_addresses:
            self.bul_sock.sendto(b'exit', addr)
        self.is_receive_characteristics = False
        self.is_receive_bullet = False


class Client:
    def __init__(self, host):
        self.server_address = (host, 13579)
        self.sock = socket(AF_INET, SOCK_DGRAM)
        self.bul_sock = socket(AF_INET, SOCK_DGRAM)
        self.is_correct_address = True
        try:
            self.sock.connect(self.server_address)
            self.bul_sock.connect((host, 13597))
        except OSError:
            self.is_correct_address = False
        self.is_receive_bullet = True

    def send(self, message):
        self.sock.sendto(message.encode('ascii'), self.server_address)

    def receive_characteristics(self):
        # self.sock.setblocking(True)
        self.sock.settimeout(0.5)
        # self.is_receive_characteristics = True
        try:
            data = self.sock.recv(1024)
            data = pickle.loads(data)
            for i in range(len(data)):
                game.players[i].deserialize(data[i])
            # bullet = Bullet()
            # bullet.deserialize(bullet_data)
            # if not bullet.is_empty():
            #     game.bullets.append(bullet)




                # players_data = pickle.loads(players_data)
                # bullet_data = pickle.loads(bullet_data)
                # for i in range(len(players_data)):
                #     game.players[i].deserialize(data[i])
                # f
                # # print('12312312312312312312321223')
                # print(data)
                # data = data.decode('utf-8').split('&&&')
                # pl_data = data[0].split('&')
                # for i in range(len(pl_data)):
                #     game.players[i].decode(pl_data[i])
                # bul_data = data[1].split('&')
                # game.bullets.clear()
                # for bul_inf in bul_data:
                #     new_bullet = Bullet()
                #     new_bullet.decode(bul_inf)
                #     game.bullets.append(new_bullet)
        except:
            pass

    def receive_message(self):
        self.sock.settimeout(0.5)
        try:
            message = self.sock.recv(1024)
            message = message.decode('utf-8')
            return message
        except:
            return ''

    def send_characteristics(self):
        # print('DEBUG send_characteristics', game.player.encode('ascii'))
        # data = game.player.encode('ascii')
        # new_bul_data = b''
        # for bul in game.new_bullets:
        #     new_bul_data += bul.encode('ascii') + b'&'
        # if new_bul_data != b'':
        #     new_bul_data = new_bul_data[:len(new_bul_data) - 1]
        # data += b'&&&' + new_bul_data
        # del_bul_data = b''
        # for bul in game.new_bullets:
        #     del_bul_data += bul.encode('ascii') + b'&'
        # if del_bul_data != b'':
        #     del_bul_data = del_bul_data[:len(del_bul_data) - 1]
        # data += b'&&&' + del_bul_data
        # print('pickle', sys.getsizeof(data))
        # print(type(data))

        data = game.player.serialize()
        self.sock.send(data)
        # data = (game.player.serialize(), game.bullet.serialize())
        # data = pickle.dumps(data)
        # self.sock.send(data)

    def send_bullet(self):
        data = game.bullet.serialize()
        self.bul_sock.send(data)

    def receive_bullet(self):
        self.is_receive_bullet = True
        self.bul_sock.setblocking(True)
        while self.is_receive_bullet:
            data = self.bul_sock.recv(1024)
            print(data)
            try:
                message = data.decode('utf-8')
                if message == 'exit':
                    game.run = False
                    self.sock.send(b'')
                    self.bul_sock.send(b'')
                    self.finish_game()
                if data == 'COUNTER-TERRORIST WIN ROUND':
                    game.counter_terrorist_number_wins += 1
                elif data == 'TERRORIST WIN ROUND':
                    game.terrorist_number_wins += 1
                else:
                    game.is_sleep = True
                    print(message)
                    for pl in game.players:
                        pl.health = 3
                    data = self.bul_sock.recv(1024)
                    print('aaaaaaaaaaaaaaaaaaaaaaa')
                    data = pickle.loads(data)
                    for i in range(len(data)):
                        game.players[i].deserialize(data[i])
            except:
                bullet = Bullet()
                bullet.deserialize(data)
                game.bullets.append(bullet)
        print('exit from client.receive_bullet')

    def send_request(self, is_accept):
        try:
            if is_accept:
                self.sock.settimeout(2)
                self.sock.send(b'Request for acceptance')
                data = self.sock.recv(1024)
                if data.decode('utf-8') == 'accept':
                    return True
            else:
                self.sock.settimeout(0.5)                       # 10 * 0.5
                self.sock.send(b'Request for exit')
                data = self.sock.recv(1024)
                if data.decode('utf-8') == 'exit':
                    return True
        except:
            print("send_request")
            return False

    def receive_prepared_message(self):                                 # True True
        message = self.sock.recv(1024)
        if message.decode('utf-8') == 'Game will create':
            return True, True
        if message.decode('utf-8') == 'Game will load':
            return True, False
        elif message.decode('utf-8') == 'Server disconnected':
            self.sock.close()
            return False, False
        else:
            return False, False

    def close_socket(self):
        for i in range(10):
            is_exit = self.send_request(False)
            if is_exit:
                break
        self.sock.close()

    def finish_game(self):
        self.is_receive_bullet = False
        self.sock.close()
        self.bul_sock.close()