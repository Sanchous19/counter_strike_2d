import math
import pygame
import screen_info as scr
import sprite
import pickle


class Player:
    def __init__(self, side, ip_address='', port='', x=0, y=0, width=43, height=24, angle=0, step=5):
        self.ip_address = ip_address
        self.port = port
        self.x_center = x
        self.y_center = y
        self.width = width
        self.height = height
        self.angle = angle
        self.step = step
        self.side = side
        self.health = 3

    def hit(self):
        self.health -= 1

    def is_kill(self):
        return True if self.health == 0 else False

    def move(self, side):
        if side == 'left':
            self.x_center -= self.step
        elif side == 'right':
            self.x_center += self.step
        elif side == 'up':
            self.y_center -= self.step
        elif side == 'down':
            self.y_center += self.step

    def calculate_angle(self, mouse_coordinates):
        x_mouse, y_mouse = mouse_coordinates
        x_mouse /= scr.coefficient_width
        y_mouse /= scr.coefficient_height
        x_head = self.x_center - self.width / 4
        y_head = self.y_center
        if x_head == x_mouse:
            if y_mouse >= y_head:
                self.angle = 90
            else:
                self.angle = 180
        elif x_mouse > x_head:
            if y_mouse <= y_head:
                self.angle = math.degrees(math.atan((y_head - y_mouse) / (x_mouse - x_head)))
            else:
                self.angle = math.degrees(2 * math.pi + math.atan((y_head - y_mouse) / (x_mouse - x_head)))
        else:
            self.angle = math.degrees(math.atan((y_head - y_mouse) / (x_mouse - x_head)) + math.pi)

    # def get_characteristics(self):
    #     return self.address, self.x, self.y, self.width, self.height

    def display(self, screen):
        if self.health == 0:
            return
        rotate_image = None
        if self.side == 'terrorist':
            rotate_image = pygame.transform.rotate(sprite.terrorist_image, self.angle)
        elif self.side == 'counter terrorist':
            rotate_image = pygame.transform.rotate(sprite.counter_terrorist_image, self.angle)
        x_head = self.x_center - self.width / 4
        y_head = self.y_center
        x = x_head + (self.x_center - x_head) * math.cos(-math.radians(self.angle)) - \
                     (self.y_center - y_head) * math.sin(-math.radians(self.angle))
        y = y_head + (self.y_center - y_head) * math.cos(-math.radians(self.angle)) + \
                     (self.x_center - x_head) * math.sin(-math.radians(self.angle))
        width = rotate_image.get_width() * scr.coefficient_width
        height = rotate_image.get_height() * scr.coefficient_height
        # print(self.x_head, self.y_head)
        # print(self.x, self.y)
        # print(scr.coefficient_width, scr.coefficient_height)
        # print(rotate_image.get_width(), rotate_image.get_height())
        screen.blit(rotate_image, (int((x - width / 2) * scr.coefficient_width),
                                   int((y - height / 2)) * scr.coefficient_height))
        # print(self.x_center, self.y_center, self.x_head, self.y_head)
        # print(self.x_head, self.y_head)
        # print(self.angle)
        # print(type(rotate_image))
        # print(self.x_head, self.y_head)
        # pygame.image.save(rotate_image, "q")

    def serialize(self):
        if self.health == 0:
            return b'KILL'
        else:
            return pickle.dumps((self.x_center, self.y_center, self.angle))

    def deserialize(self, data):
        self.x_center, self.y_center, self.angle = pickle.loads(data)
        # print(self.x_center, self.y_center, self.angle)

    # def encode(self):
    #     if self.health == 0:
    #         return b'KILL'
    #     else:
    #         return (str(self.x_center) + '###' + str(self.y_center) + '###' +
    #                 str(self.angle)).encode('ascii')
    #
    # def decode(self, data):
    #     data = data.split('###')
    #     if self.ip_address == data[0]:
    #         self.x_center = float(data[1])
    #         self.y_center = float(data[2])
    #         self.angle = float(data[3])
    #         return True
    #     else:
    #         return False
    #
    # def full_encode(self):
    #     return (str(self.ip_address) + '###' + str(self.port) + '###' + str(self.side) + '###' + str(self.x_center) +
    #             '###' + str(self.y_center)).encode('ascii')
