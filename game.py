import pygame
from player import Player
from bullet import Bullet
import sprite
import screen_info as scr
import control_socket
import threading
import pickle
import time
# from tkinter import *
# from tkinter import messagebox
import tkinter as tk
import mongo_work


# player = Player('123', 243, 'terrorist', 100, 100)
# player1 = Player('124', 243, 'terrorist', 300, 300)
# player2 = Player('125', 243, 'counter terrorist', 700, 100)
# player3 = Player('126', 243, 'counter terrorist', 100, 700)
# players = [player, player1, player2, player3]
run = True
is_sleep = False
server = None
client = None
player = None
bullet = None
index = 0
players = []
bullets = []
new_bullets = []
delete_bullets = []
border_width = 60 * scr.coefficient_width
border_height = 60 * scr.coefficient_height
border = 40
brick_walls_coordinates = []
wooden_box_coordinates = [(450, 500), (550, 800), (600, 215), (610, 500), (740, 870), (790, 300), (850, 830),
                          (900, 600), (970, 310), (1010, 790), (1090, 370), (1180, 790), (1260, 490), (1410, 340),
                          (1490, 680)]
send_server_thread = None
receive_server_thread = None
terrorist_number_wins = 0
counter_terrorist_number_wins = 0


def menu():
    menu = tk.Tk()
    menu.title('Counter Strike')

    # menu.geometry('960x540+{}+{}'.format(scr.player_width // 2 - 480, scr.player_height // 2 - 270))
    open_connect_menu_button = tk.Button(menu, text='Сохранить игру', command=mongo_work.save)
    open_connect_menu_button.pack()
    open_connect_menu_button = tk.Button(menu, text='Выйти', command=lambda: exit_from_game(menu))
    open_connect_menu_button.pack()
    menu.mainloop()
    print('exit from menu')


def save_game():
    mongo_work.save()


def exit_from_game(window):
    global run
    run = False
    if isinstance(server, control_socket.Server):
        server.finish_game()
    else:
        client.finish_game()
    window.destroy()
    window.quit()



def fun():
    client.send_characteristics()
    message = client.receive_message()
    # if message == 'TERRORIST WIN GAME':
    #     messagebox.showinfo('', 'TERRORIST WIN')
    #     pygame.quit()
    # elif message == 'COUNTER-TERRORIST WIN GAME':
    #     messagebox.showinfo('', 'COUNTER-TERRORIST WIN')
    #     pygame.quit()
    # elif message == 'COUNTER-TERRORIST WIN ROUND':
    #     for pl in players:
    #         pl.health = 3
    #     client.sock.setblocking(True)
    # elif message == 'TERRORIST WIN ROUND':
    #     for pl in players:
    #         pl.health = 3
    #     client.sock.setblocking(True)
    client.receive_characteristics()


def define_all_players():
    global player, players, index
    data = client.sock.recv(1024)
    print(data, 'weertyuio')
    index, sides, health = pickle.loads(data)
    print('define_all_players')
    for i in range(len(sides)):
        pl = Player(sides[i])
        pl.health = health[i]
        players.append(pl)
    player = players[index]
    # client.receive_characteristics()
    print('asdfjhadshafuibdsiauf')
    # address = control_socket.get_ip_address() if server is None else '127.0.0.1'
    # print(address)
    # for pl in players:
    #     if address == pl.ip_address:
    #         player = pl
    #         print('goooood')
    #         break


def check_player_move(side):
    x = player.x_center
    y = player.y_center
    if side == 'left':
        x = player.x_center - player.width / 2 - player.step
    elif side == 'right':
        x = player.x_center + player.width / 2 + player.step
    elif side == 'up':
        y = player.y_center - player.height / 2 - player.step
    elif side == 'down':
        y = player.y_center + player.height / 2 + player.step
    for pl in players:
        if player != pl and not pl.is_kill():
            if pl.x_center - pl.width <= x <= pl.x_center + pl.width and \
                    pl.y_center - pl.height <= y <= pl.y_center + pl.height:
                return False
    for coord in brick_walls_coordinates + wooden_box_coordinates:
        if coord[0] - border <= x <= coord[0] + border and coord[1] - border <= y <= coord[1] + border:
            return False
    if border_width <= x <= scr.developer_width - border_width and \
            border_height <= y <= scr.developer_height - border_height:
        return True
    else:
        return False


def check_bullet_move(bul):
    x = bul.x + bul.x_speed
    y = bul.y + bul.y_speed
    for pl in players:
        if players[bul.index] != pl and not pl.is_kill():
            if pl.x_center - pl.width <= x <= pl.x_center + pl.width and \
                    pl.y_center - pl.height <= y <= pl.y_center + pl.height:
                pl.hit()
                return False
    for coord in brick_walls_coordinates + wooden_box_coordinates:
        if coord[0] - border <= x <= coord[0] + border and coord[1] - border <= y <= coord[1] + border:
            return False
    if border_width <= x <= scr.player_width - border_width and border_height <= y <= scr.player_height - border_height:
        return True
    else:
        return False


def start_game(main_server, main_client):
    global server, client, send_server_thread, receive_server_thread, bullet, run, is_sleep
    server = main_server
    client = main_client
    # receive_client_thread.start()
    if isinstance(server, control_socket.Server):
        # send_server_thread = threading.Thread(target=server.send_characteristics)
        receive_server_thread = threading.Thread(target=server.receive_characteristics)
        # /send_server_thread.start()
        receive_server_thread.start()
        receive_bullet_server_thread = threading.Thread(target=server.receive_bullet)
        receive_bullet_server_thread.start()
    client.bul_sock.send(b'')
    receive_bullet_client_thread = threading.Thread(target=client.receive_bullet)
    receive_bullet_client_thread.start()
    # client.receive_characteristics()
    define_all_players()
    pygame.init()
    # display = pygame.display.set_mode(flags=pygame.FULLSCREEN)
    display = pygame.display.set_mode((1500, 1000))
    pygame.display.set_caption("Counter Strike")
    display.blit(sprite.background_image, (0, 0))
    for i in range(9):
        brick_walls_coordinates.append((300, 300 + 50 * i))
        brick_walls_coordinates.append((1600, 300 + 50 * i))



    # player.display(display)

    for pl in players:
        pl.display(display)

    run = True
    # ss = pickle.dumps((player.ip_address, player.x_center, player.y_center, player.angle))
    # print(ss)
    # print(type(ss))
    time.sleep(3)
    while run:
        # if isinstance(server, control_socket.Server):
        #     time.sleep(0.01)
        if is_sleep:
            time.sleep(1.5)
            is_sleep = False
        pygame.time.delay(5)
        bullet = Bullet()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                thread = threading.Thread(target=menu)
                thread.start()
                print('gfsstrdxtcjhvjbknb,jhvncbxvzxcvbjnklkjhkfhdgsf')
            if event.type == pygame.MOUSEBUTTONDOWN and not player.is_kill():
                bullet = Bullet(index)
                bullet.x, bullet.y, bullet.angle = player.x_center, player.y_center, player.angle
                client.send_bullet()

        # print('bullet', bullet.x, bullet.y, bullet.angle)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and check_player_move('left'):
            player.move('left')
        if keys[pygame.K_RIGHT] and check_player_move('right'):
            player.move('right')
        if keys[pygame.K_UP] and check_player_move('up'):
            player.move('up')
        if keys[pygame.K_DOWN] and check_player_move('down'):
            player.move('down')
        player.calculate_angle(pygame.mouse.get_pos())
        # print(pygame.mouse.get_pos())
        # display.fill((0, 0, 0))

        # th = threading.Thread()
        # th.start()

        client.send_characteristics()
        client.receive_characteristics()
        # message = client.receive_message()
        # if message == 'TERRORIST WIN GAME':
        #     messagebox.showinfo('', 'TERRORIST WIN')
        #     pygame.quit()
        # elif message == 'COUNTER-TERRORIST WIN GAME':
        #     messagebox.showinfo('', 'COUNTER-TERRORIST WIN')
        #     pygame.quit()
        # elif message == 'COUNTER-TERRORIST WIN ROUND':
        #     for pl in players:
        #         pl.health = 3
        #     client.sock.setblocking(True)
        # elif message == 'TERRORIST WIN ROUND':
        #     for pl in players:
        #         pl.health = 3
        #     client.sock.setblocking(True)
        # client.receive_characteristics()

        display.blit(sprite.background_image, (0, 0))
        for coord in brick_walls_coordinates:
            display.blit(sprite.brick_wall_image, ((coord[0] - border) * scr.coefficient_width,
                                                   (coord[1] - border) * scr.coefficient_height))
        for coord in wooden_box_coordinates:
            display.blit(sprite.wooden_box_image, ((coord[0] - border) * scr.coefficient_width,
                                                   (coord[1] - border) * scr.coefficient_height))
        for pl in players:
            if not pl.is_kill():
                pl.display(display)


        for bul in bullets:
            if check_bullet_move(bul):
                bul.move()
                bul.display(display)
            else:
                bullets.remove(bul)
                del bul


        # pygame.draw.rect(display, (0, 0, 255), (x, y, width, height))
        pygame.display.update()

    print('sfjdodhshfiuhsdifhiusdbifiusdahfcdsbivuudsbaca')
    pygame.quit()


# start_game()