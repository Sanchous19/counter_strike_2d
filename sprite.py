import pygame
import tkinter
import screen_info as scr
import PIL.Image


image = PIL.Image.open('images/terrorist_select.png')
image = image.resize((int(772 * scr.coefficient_width), int(836 * scr.coefficient_height)), PIL.Image.ANTIALIAS)
image.save('images/terrorist_select1.png')
image = PIL.Image.open('images/counter_terrorist_select.png')
image = image.resize((int(772 * scr.coefficient_width), int(836 * scr.coefficient_height)), PIL.Image.ANTIALIAS)
image.save('images/counter_terrorist_select1.png')


background_image = pygame.image.load('images/background.png')
background_image = pygame.transform.smoothscale(background_image, (scr.player_width, scr.player_height))
terrorist_image = pygame.image.load('images/terrorist.png')
terrorist_image = pygame.transform.smoothscale(terrorist_image, (int(terrorist_image.get_width() * scr.coefficient_width),
                                                                 int(terrorist_image.get_height() * scr.coefficient_height)))
counter_terrorist_image = pygame.image.load('images/counter_terrorist.png')
counter_terrorist_image = pygame.transform.smoothscale(counter_terrorist_image,
                                                       (int(counter_terrorist_image.get_width() * scr.coefficient_width),
                                                        int(counter_terrorist_image.get_height() * scr.coefficient_height)))
bullet_image = pygame.image.load('images/terrorist.png')
bullet_image = pygame.transform.smoothscale(bullet_image, (int(5 * scr.coefficient_width), int(5 * scr.coefficient_height)))
brick_wall_image = pygame.image.load('images/brick_wall.png')
brick_wall_image = pygame.transform.smoothscale(brick_wall_image, (int(50 * scr.coefficient_width), int(50 * scr.coefficient_height)))
brick_wall_image = pygame.transform.rotate(brick_wall_image, 90)
wooden_box_image = pygame.image.load('images/wooden_box.png')
wooden_box_image = pygame.transform.smoothscale(wooden_box_image, (int(50 * scr.coefficient_width), int(50 * scr.coefficient_height)))
